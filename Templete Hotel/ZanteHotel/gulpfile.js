var gulp = require('gulp');
var minifyCSS = require('gulp-csso');
var jsmin = require('gulp-jsmin');
const imagemin = require('gulp-imagemin');
var rename = require('gulp-rename');

gulp.task('css', function() {
  return gulp.src('revolution/css/*.css')
    .pipe(minifyCSS())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('assets/css'))
});

gulp.task('js', function () {
    gulp.src('revolution/js/*.js')
        .pipe(jsmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('assets/js'));
});

gulp.task('img', function() {
  gulp.src('images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('assets/images'))
});

gulp.task('default', [ 'css', 'js' ]);