
/* HOME PAGE SLIDER SCRIPTS START */

(function( $ ) {
	//Variables on page load 
	var $myCarousel = $('#carousel-example-generic'),
		$firstAnimatingElems = $myCarousel.find('.item:first').find("[data-animation ^= 'animated']");
		
	//Initialize carousel 
	$myCarousel.carousel();
	
	//Pause carousel  
	$myCarousel.carousel('pause');
	 
    $('#carousel-example-generic').carousel({
        interval:4000,
        pause: "false"
    });
	
})(jQuery);	
